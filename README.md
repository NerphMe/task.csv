Важно! Актуально только для Linux/MacOS

1) Сколнировать репозиторий в локальную папку. (git clone https://gitlab.com/NerphMe/task.csv.git)
2) Установить Mysql и создать там юзера с логином и паролем root root и названием базы new и таблоцей csv. Ниже приведем код для генерирования схемы и таблицы.
create schema new;
create table csv
(
    uid    varchar(244) null,
    name   varchar(244) null,
    email  varchar(244) null,
    age    varchar(244) null,
    phone  varchar(244) null,
    gender varchar(255) null
);

3) Запустить встроеный веб-сервер в локальной папке (php -S localhost:8000)
4) Перейти на http://localhost:8000/upload.php и получить удовольствие от использования проекта.