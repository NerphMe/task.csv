<!DOCTYPE html>
<html>
<head>
    <title>Table with database</title>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
            color: #588c7e;
            font-family: monospace;
            font-size: 25px;
            text-align: left;
        }

        th {
            background-color: #588c7e;
            color: white;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2
        }
    </style>
</head>
<body>
<table>
    <tr>
        <th>uid</th>
        <th>name</th>
        <th>email</th>
        <th>age</th>
        <th>phone</th>
        <th>gender</th>
    </tr>
    <?php
    $conn = mysqli_connect("localhost", "root", "root", "new");
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $sql = "SELECT * FROM csv ORDER BY uid DESC";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            echo "<tr><td>" . $row["uid"] . "</td><td>" . $row["name"] . "</td><td>"
                . $row["email"] . "</td><td>" . $row["age"] . "</td><td>" . $row["phone"] . "</td><td>" . $row["gender"] . "</td></tr>";
        }
        echo "</table>";
        }
    else {
            echo "0 results";


    }

    ?>
    <form method='post' action='download.php'>
        <input type='submit' value='Export' name='Export'>
</table>
</body>
</html>
